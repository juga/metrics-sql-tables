-- Refresh materialized views
-- ==========================

-- The materialized views are being refreshed by
-- [descriptorParser](https://gitlab.torproject.org/tpo/network-health/metrics/descriptorParser/-/blob/f4d479b4aae6a7f62a3893df197e51856b3ad552/src/main/java/org/torproject/metrics/descriptorparser/Main.java),
-- but having this file too to be able to refresh them without
-- descriptorParser.

-- From onionperf_views.sql (on network_status changes)
REFRESH MATERIALIZED VIEW CONCURRENTLY relay_throughput_avg_filt;
REFRESH MATERIALIZED VIEW CONCURRENTLY net_throughput;
REFRESH MATERIALIZED VIEW CONCURRENTLY relay_throughput_ratio;
REFRESH MATERIALIZED VIEW CONCURRENTLY onionperf_nodes;

-- From network_status_churn_views.sql (on network_status changes, each hour)
REFRESH MATERIALIZED VIEW CONCURRENTLY hourly_relays_cw_flags;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_guard;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_exit;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_hsdir;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_stable;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_valid;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_v2dir;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_new;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_new_guard;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_new_exit;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_new_hsdir;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_new_stable;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_new_valid;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_churn_rate_new_v2dir;

-- From network_status_cw_churn_views.sql (on network_status changes, each hour)
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_guard;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_exit;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_hsdir;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_stable;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_valid;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_v2dir;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_new;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_new_guard;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_new_exit;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_new_hsdir;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_new_stable;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_new_valid;
REFRESH MATERIALIZED VIEW CONCURRENTLY relays_cw_churn_rate_new_v2dir;

-- From cdf_bandwidth_views.sql (on bandwidth_file changes, each hour)
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_bastet;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_bastet_previous_month;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_bastet_previous_2weeks;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_bastet_previous_week;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_gabelmoo;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_gabelmoo_previous_month;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_gabelmoo_previous_2weeks;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_gabelmoo_previous_week;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_longclaw;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_longclaw_previous_month;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_longclaw_previous_2weeks;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_longclaw_previous_week;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_maatuska;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_maatuska_previous_month;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_maatuska_previous_2weeks;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_maatuska_previous_week;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_tor26;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_tor26_previous_month;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_tor26_previous_2weeks;
REFRESH MATERIALIZED VIEW CONCURRENTLY cdf_tor26_previous_week;

-- From server_status_latest_views.sql (on server_status changes, each hour)
REFRESH MATERIALIZED VIEW CONCURRENTLY server_status_latest;