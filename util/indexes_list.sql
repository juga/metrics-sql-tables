-- List all indexes
-- ================

SELECT
    c.relname AS table_name,
    i.indexrelid::regclass AS index_name,
    i.indisprimary AS is_pk,
    i.indisunique AS is_unique
FROM pg_index i
join pg_class c ON c.oid = i.indrelid
WHERE c.relnamespace::regnamespace::TEXT = 'public';
