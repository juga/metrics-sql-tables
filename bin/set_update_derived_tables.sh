#!/bin/bash

set -x 

SCRIPT_PATH=$(dirname "${BASH_SOURCE[0]}")
PROJECT_PATH=$(dirname "$SCRIPT_PATH")
mkdir -p $HOME/.config/systemd/user
cp "$PROJECT_PATH"/systemd/user/* "$HOME"/.config/systemd/user/
export XDG_RUNTIME_DIR=/run/user/$(id -u)

systemctl --user daemon-reload
systemctl --user enable --now parser_update.timer

# To check it works
# systemctl --user --now start parser_update.service
# systemctl --user --list-timers
# journalctl --user -u parser_update