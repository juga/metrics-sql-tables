CREATE TABLE IF NOT EXISTS onionperf_analysis(
  onionperf_node                    TEXT                          NOT NULL,
  published                         TIMESTAMP WITHOUT TIME ZONE   NOT NULL,
  measurement_ip                    TEXT                          NOT NULL,
  type                              TEXT                          NOT NULL,
  version                           TEXT                          NOT NULL,
  filters                           TEXT                          NOT NULL,
  digest                            TEXT                          NOT NULL,
  PRIMARY KEY(digest)
);

CREATE index ON onionperf_analysis(published);

CREATE TABLE IF NOT EXISTS onionperf_tgen_stream(
  onionperf_node                        TEXT                          NOT NULL,
  stream_id                             TEXT                          NOT NULL,
  byte_info_payload_bytes_recv          BIGINT,
  byte_info_payload_bytes_send          BIGINT,
  byte_info_payload_progress_recv       TEXT,
  byte_info_payload_progress_send       TEXT,
  byte_info_total_bytes_recv            BIGINT,
  byte_info_total_bytes_send            BIGINT,
  elapsed_seconds_payload_bytes_recv    TEXT,
  elapsed_seconds_payload_bytes_send    TEXT,
  elapsed_seconds_payload_progress_recv TEXT,
  elapsed_seconds_payload_progress_sent TEXT,
  is_complete                           BOOLEAN,
  is_error                              BOOLEAN,
  is_success                            BOOLEAN,
  stream_info_error                     TEXT,
  stream_info_id                        BIGINT,
  stream_info_name                      TEXT,
  stream_info_peername                  TEXT,
  stream_info_recvsize                  BIGINT,
  stream_info_recvstate                 TEXT,
  stream_info_sendsize                  BIGINT,
  stream_info_sendstate                 TEXT,
  stream_info_vertexid                  TEXT,
  time_info_created_ts                  BIGINT,
  time_info_now_ts                      BIGINT,
  time_info_usecs_to_checksum_recv      BIGINT,
  time_info_usecs_to_checksum_send      BIGINT,
  time_info_usecs_to_command            BIGINT,
  time_info_usecs_to_first_byte_recv    BIGINT,
  time_info_usecs_to_first_byte_send    BIGINT,
  time_info_usecs_to_last_byte_recv     BIGINT,
  time_info_usecs_to_last_byte_send     BIGINT,
  time_info_usecs_to_proxy_choice       BIGINT,
  time_info_usecs_to_proxy_init         BIGINT,
  time_info_usecs_to_proxy_request      BIGINT,
  time_info_usecs_to_proxy_response     BIGINT,
  time_info_usecs_to_response           BIGINT,
  time_info_usecs_to_socket_connect     BIGINT,
  time_info_usecs_to_socket_create      BIGINT,
  transport_info_error                  TEXT,
  transport_info_fd                     BIGINT,
  transport_info_local                  TEXT,
  transport_info_proxy                  TEXT,
  transport_info_remote                 TEXT,
  transport_info_state                  TEXT,
  unix_ts_end                           TIMESTAMP WITHOUT TIME ZONE,
  unix_ts_start                         TIMESTAMP WITHOUT TIME ZONE,
  onionperf_analysis                    TEXT references onionperf_analysis(digest),
  PRIMARY KEY(onionperf_node, stream_id, unix_ts_start)
);

CREATE TABLE IF NOT EXISTS onionperf_tor_circuit(
  onionperf_node                    TEXT                          NOT NULL,
  circuit_id                        TEXT                          NOT NULL,
  build_quantile                    DOUBLE PRECISION,
  build_timeout                     BIGINT,
  buildtime_seconds                 DOUBLE PRECISION,
  cbt_set                           BOOLEAN,
  current_guards                    TEXT,
  elapsed_seconds                   TEXT,
  failure_reason_local              TEXT,
  filtered_out                      BOOLEAN,
  circuit_path                      TEXT,
  unix_ts_end                       TIMESTAMP WITHOUT TIME ZONE,
  unix_ts_start                     TIMESTAMP WITHOUT TIME ZONE,
  onionperf_analysis                TEXT references onionperf_analysis(digest),
  PRIMARY KEY(onionperf_node, circuit_id, unix_ts_start)
);

CREATE index ON onionperf_tor_circuit(onionperf_analysis);

CREATE TABLE IF NOT EXISTS onionperf_measurement(
  onionperf_node                     TEXT                          NOT NULL,
  stream_id                          TEXT                          NOT NULL,
  tor_stream_id                      TEXT                          NOT NULL,
  circuit_id                         TEXT                          NOT NULL,
  filesize_bytes                     INTEGER,
  payload_progress_100               DOUBLE PRECISION,
  payload_progress_80                DOUBLE PRECISION,
  time_to_first_byte                 BIGINT,
  time_to_last_byte                  BIGINT,
  error_code                         TEXT,
  endpoint_local                     TEXT,
  unix_ts_end                        TIMESTAMP WITHOUT TIME ZONE,
  unix_ts_start                      TIMESTAMP WITHOUT TIME ZONE,
  mbps                               DOUBLE PRECISION,
  path                               TEXT,
  cbt_set                            BOOLEAN,
  filtered_out                       BOOLEAN,
  is_onion                           BOOLEAN,
  middle                             TEXT,
  guard                              TEXT,
  exit                               TEXT,
  onionperf_analysis                 TEXT references onionperf_analysis(digest),
  PRIMARY KEY(onionperf_node, circuit_id, stream_id, unix_ts_start)
);

CREATE TABLE IF NOT EXISTS onionperf_tor_guard(
  onionperf_node                        TEXT                          NOT NULL,
  country                               TEXT,
  dropped_ts                            TIMESTAMP WITHOUT TIME ZONE,
  fingerprint                           TEXT,
  nickname                              TEXT,
  onionperf_analysis                    TEXT references onionperf_analysis(digest),
  PRIMARY KEY(onionperf_node, fingerprint, nickname, dropped_ts)
);

CREATE TABLE IF NOT EXISTS onionperf_tor_stream(
  onionperf_node                        TEXT                          NOT NULL,
  stream_id                             TEXT,
  circuit_id                            TEXT,
  elapsed_seconds                       TEXT,
  source                                TEXT,
  target                                TEXT,
  unix_ts_end                           TIMESTAMP WITHOUT TIME ZONE,
  unix_ts_start                         TIMESTAMP WITHOUT TIME ZONE,
  onionperf_analysis                    TEXT references onionperf_analysis(digest),
  PRIMARY KEY(onionperf_node, stream_id, circuit_id, unix_ts_start)
);
