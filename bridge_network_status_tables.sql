CREATE TABLE IF NOT EXISTS bridge_network_status(
  published               TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  fingerprint             TEXT                         NOT NULL,
  flag_thresholds         TEXT,
  stable_uptime           BIGINT,
  stable_mtbf             BIGINT,
  fast_bandwidth          BIGINT,
  guard_wfu               DOUBLE PRECISION,
  guard_tk                BIGINT,
  guard_bandwidth_including_exits BIGINT,
  guard_bandwidth_excluding_exits BIGINT,
  enough_mtbf_info        INTEGER,
  ignore_adv_bws          INTEGER,
  header                  TEXT,
  digest                  TEXT                         NOT NULL,
  PRIMARY KEY(digest));

CREATE index ON bridge_network_status(published);

CREATE TABLE IF NOT EXISTS bridge_network_status_entry(
  published               TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  fingerprint             TEXT                         NOT NULL,
  nickname                TEXT                         NOT NULL,
  digest                  TEXT                         NOT NULL,
  network_status          TEXT references bridge_network_status(digest),
  address                 TEXT,
  or_port                 INTEGER,
  dir_port                INTEGER,
  or_address              TEXT,
  flags                   TEXT,
  bandwidth               BIGINT,
  policy                  TEXT,
  PRIMARY KEY(digest));

CREATE TABLE IF NOT EXISTS bridge_network_status_document(
  bridge_network_status_digest             TEXT references bridge_network_status(digest),
  bridge_network_status_entry_digest       TEXT references bridge_network_status_entry(digest)
);

CREATE index bridge_network_status_entry_published ON bridge_network_status_entry(published);
CREATE index bridge_network_status_entry_fingerprint ON bridge_network_status_entry(fingerprint);
CREATE index bridge_network_status_entry_nickname ON bridge_network_status_entry(nickname);
CREATE INDEX bridge_status_fingerprint_nickname_published_asc_index ON bridge_network_status_entry (fingerprint, nickname, published ASC);
CREATE INDEX bridge_status_fingerprint_nickname_published_desc_index ON bridge_network_status_entry (fingerprint, nickname, published DESC);
