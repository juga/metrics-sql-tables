-- Relays' Churn Rate CREATE views, functions
-- ==========================================

-- Materlialized views are needed to create indexes
-- Any view inherits the indexes of the original tables

-- It seems there is not just one definition of Churn Rate and most
-- bibliography define Churn in terms of "Customers".
-- Applying common definitions of Customer Churn Rate (CCR) to "Relays", we
-- could define Relays Churn Rate (RCR) as:
-- RCR = LR / PSR
-- LR: number of Lost Relays at the end of the period (taking into account
-- their concrete fingerprint)
-- PSR: Period Start number of Relays
-- The period is one hour (every consensus)
-- Defining also:
-- PER: Period End number of Relays
-- NR: number of New Relays at the end of the period (taking into account their
-- concrete fingerprint)
--
-- From https://ensa.fi/papers/sybilhunting-sec16.pdf:
-- > To quantify the churn rate α between two subsequent
-- > consensus documents, we adapt Godfrey et al.’s formula,
-- > which yields a churn value that captures both systems
-- > that joined and systems that left the network
-- > However, an unusually low number of systems that left
-- > could cancel out an unusually high number of new sys-
-- > tems and vice versa—an undesired property for a tech-
-- > nique that should spot abnormal changes. To address
-- > this issue, we split the formula in two parts, creating a
-- > time series for new relays (αn ) and for relays that left
-- > (αl ). Ct is the network consensus at time t, and \ denotes
-- > the complement between two consensuses, i.e., the relays
-- > that are in the left operand, but not the right operand. We
-- > define αn and αl as
-- >
-- > alpha_n = |Ct\Ct-1| / |Ct|
-- > alpha_l = |Ct-1\Ct| / |Ct-1|
-- >
-- > Both αn and αl are bounded to the interval [0, 1]. A
-- > churn value of 0 indicates no change between two subse-
-- > quent consensuses whereas a churn value of 1 indicates
-- > a complete turnover. Determining αn,l for the sequence
-- > Ct ,Ct−1 , . . . , Ct−n , yields a time series of churn values
-- > that can readily be inspected for abnormal spikes.
-- >
-- > We found that many churn anomalies are caused
-- > by relays that share a flag, or a flag combination, e.g.,
-- > HSDir (onion service directories) and Exit (exit relays).
-- > Therefore, sybilhunter can also generate per-flag churn
-- > time series that can uncover patterns that would be lost
-- > in a flag-agnostic time series.

-- With this definitions, we have these equivalences:
-- PSR = |Ct-1|
-- PER = |Ct|
-- LR = |Ct-1\Ct|
-- NR = |Ct\Ct-1|
-- alpha_l = |Ct-1\Ct| / |Ct-1|  = LR / PSR = RCR
-- alpha_n = |Ct\Ct-1| / |Ct| = NR / PER = RCRN (Churn with New Relays, not a
-- good term)

-- Note that as stated above, the definition FROM
-- https://people.eecs.berkeley.edu/~istoica/papers/2006/churn.pdf, is
-- different:
-- C = 1 / T ∑ ( |Ui-1 ⊖ Ui| / max{|Ui-1|, |Ui|} )
-- If we take 1 / T = 1h, then:
-- C = |Ct-1 ⊖ Ct| / max{|Ct-1|, |Ct|})
--   = |(Ct-1\Ct) U (Ct\Ct-1)| / max{|Ct-1|, |Ct|}
--   = LR U NR / max{PSR, PER}
-- Which is not alpha_n nor alpha_l

-- Note also that here is not applied the `moving average`, explained in
-- https://ensa.fi/papers/sybilhunting-sec16.pdf:

-- > Finally, to detect changes in the underlying time se-
-- > ries trend—flat hills—we can smooth αn,l using a simple
-- > moving average λ defined as
-- >
-- > λ=1/w  ∑ αi
-- >
-- > As we increase the window size w, we can detect more
-- > subtle changes in the underlying churn trend. If λ or αn,l
-- > exceed a manually defined threshold, an alert is raised.
-- > Section 5.3 elaborates on how we can select a threshold
-- > in practice.


CREATE MATERIALIZED VIEW IF NOT EXISTS hourly_relays_cw_flags AS
SELECT DISTINCT
    network_status.valid_after AS valid_after_hour,
    network_status_entry.fingerprint,
    network_status_entry.flags,
    network_status_entry.bandwidth_weight AS cw
FROM network_status
INNER JOIN network_status_document
    ON network_status.digest
        = network_status_document.network_status_digest
INNER JOIN network_status_entry
    ON network_status_entry.digest
        = network_status_document.network_status_entry_digest
ORDER BY valid_after_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS hourly_relays_cw_flags_valid_after_hour_fingerprint_idx ON hourly_relays_cw_flags(valid_after_hour, fingerprint);

-- LR = Ct-1\Ct
-- SELECT * FROM hourly_lost_relays_by_flag('Guard')
-- WHERE end_hour BETWEEN '2024-04-01' AND '2024-04-01 02:00';
--       end_hour       | lr
-- ---------------------+----
--  2024-04-01 00:00:00 |  2
--  2024-04-01 01:00:00 |  8
--  2024-04-01 02:00:00 |  5
CREATE OR REPLACE FUNCTION hourly_lost_relays_by_flag (flag TEXT)
RETURNS TABLE (
    end_hour TIMESTAMP WITHOUT TIME ZONE,
    lr INT
) AS
$body$
SELECT
    hourly_relays_cw_flags.valid_after_hour + INTERVAL '1 hour' AS end_hour,
    count(hourly_relays_cw_flags.fingerprint) AS lr
FROM hourly_relays_cw_flags
LEFT JOIN hourly_relays_cw_flags AS end_relays
    ON end_relays.fingerprint = hourly_relays_cw_flags.fingerprint
        AND end_relays.valid_after_hour
        = hourly_relays_cw_flags.valid_after_hour + INTERVAL '1 hour'
WHERE end_relays.fingerprint IS NULL
AND hourly_relays_cw_flags.flags like '%' || flag || '%'
GROUP BY hourly_relays_cw_flags.valid_after_hour
ORDER BY hourly_relays_cw_flags.valid_after_hour
$body$
LANGUAGE sql;

-- PSR = Ct-1
-- SELECT *
-- FROM hourly_number_relays_by_flag('Guard')
-- WHERE end_hour BETWEEN '2024-04-01' AND '2024-04-01 02:00';
--      start_hour      |      end_hour       | psr
-- ---------------------+---------------------+------
--  2024-03-31 23:00:00 | 2024-04-01 00:00:00 | 4107
--  2024-04-01 00:00:00 | 2024-04-01 01:00:00 | 4116
--  2024-04-01 01:00:00 | 2024-04-01 02:00:00 | 4112
CREATE OR REPLACE FUNCTION hourly_number_relays_by_flag (flag TEXT)
RETURNS TABLE (
    start_hour TIMESTAMP WITHOUT TIME ZONE,
    end_hour TIMESTAMP WITHOUT TIME ZONE,
    psr INT
) AS
$body$
SELECT
    valid_after_hour AS start_hour,
    valid_after_hour + INTERVAL '1 hour' AS end_hour,
    count(fingerprint) AS psr
FROM hourly_relays_cw_flags
WHERE flags like '%' || flag || '%'
GROUP BY valid_after_hour
ORDER BY valid_after_hour
$body$
LANGUAGE sql;

-- RCR = alpha_l = LR / PSR = |Ct-1\Ct| / |Ct-1|
-- SELECT * FROM relays_churn_rate
-- WHERE end_hour BETWEEN '2024-04-01' AND '2024-04-01 02:00';
--       end_hour       |          churn_lost
-- ---------------------+------------------------
--  2024-04-01 00:00:00 | 0.00398618123837363805
--  2024-04-01 01:00:00 | 0.00703851261620185923
--  2024-04-01 02:00:00 | 0.00407894736842105263
CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate AS
SELECT
    hnlr.end_hour,
    hnlr.lr * 1.0 / hnr.psr AS churn_lost
FROM hourly_lost_relays_by_flag('') as hnlr
INNER JOIN hourly_number_relays_by_flag('') as hnr
    ON hnlr.end_hour = hnr.end_hour
ORDER BY hnlr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_end_hour_churn_lost_idx ON relays_churn_rate(end_hour, churn_lost);

-- SELECT * FROM relays_churn_rate_guard
-- WHERE end_hour BETWEEN '2024-04-01' AND '2024-04-01 02:00';
--       end_hour       |          churn_lost
-- ---------------------+------------------------
--  2024-04-01 00:00:00 | 0.00048697345994643292
--  2024-04-01 01:00:00 | 0.00194363459669582119
--  2024-04-01 02:00:00 | 0.00121595330739299611
CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_guard AS
SELECT
    hnlr.end_hour AS end_hour,
    hnlr.lr * 1.0 / hnr.psr AS churn_lost
FROM hourly_lost_relays_by_flag('Guard') AS hnlr
INNER JOIN hourly_number_relays_by_flag('Guard') AS hnr
    ON hnlr.end_hour = hnr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_guard_end_hour_churn_lost_idx ON relays_churn_rate_guard(end_hour, churn_lost);

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_exit AS
SELECT
    hnlr.end_hour AS end_hour,
    hnlr.lr * 1.0 / hnr.psr AS churn_lost
FROM hourly_lost_relays_by_flag('Exit') AS hnlr
INNER JOIN hourly_number_relays_by_flag('Exit') AS hnr
    ON hnlr.end_hour = hnr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_exit_end_hour_churn_lost_idx ON relays_churn_rate_exit(end_hour, churn_lost);

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_hsdir AS
SELECT
    hnlr.end_hour AS end_hour,
    hnlr.lr * 1.0 / hnr.psr AS churn_lost
FROM hourly_lost_relays_by_flag('HSDir') AS hnlr
INNER JOIN hourly_number_relays_by_flag('HSDir') AS hnr
    ON hnlr.end_hour = hnr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_hsdir_end_hour_churn_lost_idx ON relays_churn_rate_hsdir(end_hour, churn_lost);

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_stable AS
SELECT
    hnlr.end_hour AS end_hour,
    hnlr.lr * 1.0 / hnr.psr AS churn_lost
FROM hourly_lost_relays_by_flag('Stable') AS hnlr
INNER JOIN hourly_number_relays_by_flag('Stable') AS hnr
    ON hnlr.end_hour = hnr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_stable_end_hour_churn_lost_idx ON relays_churn_rate_stable(end_hour, churn_lost);

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_valid AS
SELECT
    hnlr.end_hour AS end_hour,
    hnlr.lr * 1.0 / hnr.psr AS churn_lost
FROM hourly_lost_relays_by_flag('Valid') AS hnlr
INNER JOIN hourly_number_relays_by_flag('Valid') AS hnr
    ON hnlr.end_hour = hnr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_valid_end_hour_churn_lost_idx ON relays_churn_rate_valid(end_hour, churn_lost);

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_v2dir AS
SELECT
    hnlr.end_hour AS end_hour,
    hnlr.lr * 1.0 / hnnr.psr AS churn_lost
FROM hourly_lost_relays_by_flag('V2Dir') AS hnlr
INNER JOIN hourly_number_relays_by_flag('V2Dir') AS hnnr
    ON hnlr.end_hour = hnnr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_v2dir_end_hour_churn_lost_idx ON relays_churn_rate_v2dir(end_hour, churn_lost);

-- NR = |Ct\Ct-1|
-- SELECT * FROM hourly_new_relays_by_flag('Guard')
-- WHERE end_hour BETWEEN '2024-04-01' AND '2024-04-01 02:00';
--       end_hour       | nr
-- ---------------------+----
--  2024-04-01 00:00:00 |  6
--  2024-04-01 01:00:00 |  3
--  2024-04-01 02:00:00 |  5
CREATE OR REPLACE FUNCTION hourly_new_relays_by_flag (flag TEXT)
RETURNS TABLE (
    end_hour TIMESTAMP WITHOUT TIME ZONE,
    nr INT
) AS
$body$
SELECT
    end_relays.valid_after_hour AS end_hour,
    count(end_relays.fingerprint) AS nr
FROM hourly_relays_cw_flags
RIGHT JOIN hourly_relays_cw_flags AS end_relays
    ON end_relays.fingerprint = hourly_relays_cw_flags.fingerprint
        AND end_relays.valid_after_hour
        = hourly_relays_cw_flags.valid_after_hour + INTERVAL '1 hour'
WHERE hourly_relays_cw_flags.fingerprint IS NULL
AND end_relays.flags like '%' || flag || '%'
GROUP BY end_relays.valid_after_hour
ORDER BY end_relays.valid_after_hour
$body$
LANGUAGE sql;

-- PER = |Ct|
-- SELECT * FROM hourly_start_end_number_relays_by_flag('Guard')
-- WHERE start_hour BETWEEN '2024-04-01' AND '2024-04-01 02:00';
--      start_hour      |      end_hour       | psr  | per
-- ---------------------+---------------------+------+------
--  2024-04-01 00:00:00 | 2024-04-01 01:00:00 | 4116 | 4112
--  2024-04-01 01:00:00 | 2024-04-01 02:00:00 | 4112 | 4114
--  2024-04-01 02:00:00 | 2024-04-01 03:00:00 | 4114 | 4119
CREATE OR REPLACE FUNCTION hourly_start_end_number_relays_by_flag (flag TEXT)
RETURNS TABLE (
    start_hour TIMESTAMP WITHOUT TIME ZONE,
    end_hour TIMESTAMP WITHOUT TIME ZONE,
    psr INT,
    per INT
) AS
$body$
SELECT
    psr.start_hour,
    psr.end_hour,
    psr.psr AS psr,
    per.psr AS per
FROM hourly_number_relays_by_flag(flag) AS psr
INNER JOIN hourly_number_relays_by_flag(flag) AS per
    ON psr.end_hour = per.start_hour
$body$
LANGUAGE sql;

-- alpha_n = |Ct\Ct-1| / |Ct|
-- SELECT * FROM relays_churn_rate_new
-- WHERE end_hour BETWEEN '2024-04-01' AND '2024-04-01 02:00';
--       end_hour       |         churn_new
-- ---------------------+------------------------
--  2024-04-01 00:00:00 | 0.00451527224435590969
--  2024-04-01 01:00:00 | 0.01618421052631578947
--  2024-04-01 02:00:00 | 0.00486458059426768341
CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_new AS
SELECT
    hsenr.end_hour,
    hnnr.nr * 1.0
    / hsenr.per AS churn_new
FROM hourly_new_relays_by_flag('') AS hnnr
INNER JOIN hourly_start_end_number_relays_by_flag('') AS hsenr
    ON hnnr.end_hour = hsenr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_new_end_hour_churn_lost_idx ON relays_churn_rate_new(end_hour, churn_new);

-- SELECT * FROM relays_churn_rate_new_guard
-- WHERE end_hour BETWEEN '2024-04-01' AND '2024-04-01 02:00';
--       end_hour       |         churn_new
-- ---------------------+------------------------
--  2024-04-01 00:00:00 | 0.00145772594752186589
--  2024-04-01 01:00:00 | 0.00072957198443579767
--  2024-04-01 02:00:00 | 0.00121536217792902285
CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_new_guard AS
SELECT
    hsenr.end_hour AS end_hour,
    hnnr.nr * 1.0 / hsenr.per AS churn_new
FROM hourly_new_relays_by_flag('Guard') AS hnnr
INNER JOIN hourly_start_end_number_relays_by_flag('Guard') AS hsenr
    ON hnnr.end_hour = hsenr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_new_guard_end_hour_churn_lost_idx ON relays_churn_rate_new_guard(end_hour, churn_new);

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_new_exit AS
SELECT
    hsenr.end_hour AS end_hour,
    hnnr.nr * 1.0 / hsenr.per AS churn_new
FROM hourly_new_relays_by_flag('Exit') AS hnnr
INNER JOIN hourly_start_end_number_relays_by_flag('Exit') AS hsenr
    ON hnnr.end_hour = hsenr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_new_exit_end_hour_churn_lost_idx ON relays_churn_rate_new_exit(end_hour, churn_new);

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_new_hsdir AS
SELECT
    hsenr.end_hour AS end_hour,
    hnnr.nr * 1.0 / hsenr.per AS churn_new
FROM hourly_new_relays_by_flag('HSDir') AS hnnr
INNER JOIN hourly_start_end_number_relays_by_flag('HSDir') AS hsenr
    ON hnnr.end_hour = hsenr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_new_hsdir_end_hour_churn_lost_idx ON relays_churn_rate_new_hsdir(end_hour, churn_new);

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_new_stable AS
SELECT
    hsenr.end_hour AS end_hour,
    hnnr.nr * 1.0 / hsenr.per AS churn_new
FROM hourly_new_relays_by_flag('Stable') AS hnnr
INNER JOIN hourly_start_end_number_relays_by_flag('Stable') AS hsenr
    ON hnnr.end_hour = hsenr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_new_stable_end_hour_churn_lost_idx ON relays_churn_rate_new_stable(end_hour, churn_new);

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_new_valid AS
SELECT
    hsenr.end_hour AS end_hour,
    hnnr.nr * 1.0 / hsenr.per AS churn_new
FROM hourly_new_relays_by_flag('Valid') AS hnnr
INNER JOIN hourly_start_end_number_relays_by_flag('Valid') AS hsenr
    ON hnnr.end_hour = hsenr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_new_valid_end_hour_churn_lost_idx ON relays_churn_rate_new_valid(end_hour, churn_new);

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_churn_rate_new_v2dir AS
SELECT
    hsenr.end_hour AS end_hour,
    hnnr.nr * 1.0 / hsenr.per AS churn_new
FROM hourly_new_relays_by_flag('V2Dir') AS hnnr
INNER JOIN hourly_start_end_number_relays_by_flag('V2Dir') AS hsenr
    ON hnnr.end_hour = hsenr.end_hour
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS relays_churn_rate_new_v2dir_end_hour_churn_lost_idx ON relays_churn_rate_new_v2dir(end_hour, churn_new);
