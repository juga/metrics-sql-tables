-- Inflation detection flags DROP views, functions
-- ===============================================

DROP VIEW IF EXISTS relay_type CASCADE;
DROP VIEW IF EXISTS flags CASCADE;
DROP FUNCTION IF EXISTS distinct_flags CASCADE;
DROP FUNCTION IF EXISTS sum_relays_flag CASCADE;
