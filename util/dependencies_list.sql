-- List tables and materialized views dependencies
-- ===============================================

-- Get the tables references between them in DOT language
SELECT
    '"' || tc.table_name || '" -> "' || ccu.table_name || '";'
FROM
    information_schema.table_constraints AS tc
JOIN information_schema.key_column_usage AS kcu ON
    tc.constraint_name = kcu.constraint_name
    AND tc.table_schema = kcu.table_schema
    AND tc.table_schema = 'public'
JOIN information_schema.constraint_column_usage AS ccu ON
    ccu.constraint_name = tc.constraint_name
WHERE constraint_type = 'FOREIGN KEY';
