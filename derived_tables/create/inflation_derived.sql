-- Tables to speed up inflation queries

CREATE TABLE IF NOT EXISTS highest_advertised_bw_lowest_ratio_partitioned (
    -- id SERIAL PRIMARY KEY,
    month_date DATE,  -- PRIMARY KEY,
    fingerprint TEXT,
    scanner_country TEXT,
    dirauth_nickname TEXT,
    ratio_avg DOUBLE PRECISION,
    advertised_bandwidth_avg BIGINT,
    UNIQUE (month_date, fingerprint, dirauth_nickname, scanner_country)
) PARTITION BY RANGE (month_date);

CREATE OR REPLACE FUNCTION create_monthly_highest_advertised_bw_lowest_ratio_partitions(
  start_date DATE, end_date DATE) RETURNS VOID AS $$
DECLARE
    create_query TEXT;
    index_query TEXT;
BEGIN
    FOR create_query, index_query IN
        SELECT
            'CREATE TABLE highest_advertised_bw_lowest_ratio_' || TO_CHAR(d, 'YYYY_MM') || ' PARTITION OF
              highest_advertised_bw_lowest_ratio_partitioned FOR VALUES FROM (''' || TO_CHAR(d, 'YYYY-MM-01') || ''') TO (''' || TO_CHAR(d + INTERVAL '1 month', 'YYYY-MM-01') || ''');',
            'CREATE INDEX highest_advertised_bw_lowest_ratio_' || TO_CHAR(d, 'YYYY_MM') || '_idx
              ON highest_advertised_bw_lowest_ratio_' || TO_CHAR(d, 'YYYY_MM') || ' (month_date);'
        FROM generate_series(start_date, end_date, INTERVAL '1 month') AS d
    LOOP
        EXECUTE create_query;
        EXECUTE index_query;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE IF NOT EXISTS lowest_ratio_highest_advertised_bw_partitioned (
    -- id SERIAL PRIMARY KEY,
    month_date DATE,  -- PRIMARY KEY,
    fingerprint TEXT,
    scanner_country TEXT,
    dirauth_nickname TEXT,
    ratio_avg DOUBLE PRECISION,
    advertised_bandwidth_avg BIGINT,
    UNIQUE (month_date, fingerprint, dirauth_nickname, scanner_country)
) PARTITION BY RANGE (month_date);

CREATE OR REPLACE FUNCTION create_monthly_lowest_ratio_highest_advertised_bw_partitions(
  start_date DATE, end_date DATE) RETURNS VOID AS $$
DECLARE
    create_query TEXT;
    index_query TEXT;
BEGIN
    FOR create_query, index_query IN
        SELECT
            'CREATE TABLE lowest_ratio_highest_advertised_bw_' || TO_CHAR(d, 'YYYY_MM') || ' PARTITION OF
              lowest_ratio_highest_advertised_bw_partitioned FOR VALUES
              FROM (''' || TO_CHAR(d, 'YYYY-MM-01') || ''') TO (''' || TO_CHAR(d + INTERVAL '1 month', 'YYYY-MM-01') || ''');',
            'CREATE INDEX lowest_ratio_highest_advertised_bw_' || TO_CHAR(d, 'YYYY_MM') || '_idx
              ON lowest_ratio_highest_advertised_bw_' || TO_CHAR(d, 'YYYY_MM') || ' (month_date);'
        FROM generate_series(start_date, end_date, INTERVAL '1 month') AS d
    LOOP
        EXECUTE create_query;
        EXECUTE index_query;
    END LOOP;
END;
$$ LANGUAGE plpgsql;
