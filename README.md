# Metrics SQL Tables

This project contains metrics SQL tables that are used to store Tor network
documents.

These are used by several projects across the Tor metrics ecosystem.

Edit the SQL tables files if you need to modify any of the schema.
The ci build will generate a schema.hcl file to use with [atlas](https://atlasgo.io) to handle
migrations.

The changes are then applied with:
```
atlas schema apply -u "postgres://<dbuser>:<dbpassword>@localhost:5432/parser?sslmode=disable" --to file://sql/schema.hcl
```

The directory [`views_functions`](views_functions/) contains PostgreSQL
(materialized) views and functions using the tables to monitor, analyze and/or
visualize (with [grafana-dashboards](https://gitlab.torproject.org/tpo/tpa/grafana-dashboards))
different relay bandwidth values, relays churn, etc.

The directory [`views_functions/create`](views_functions/create) contains the
PostgreSQL statements to create the (materialized) views, functions and indexes
. Similarly, the [`views_functions/drop`](views_functions/drop) contains the
statements to drop them.

The file
[`views_functions/materialized_views_refresh.sql`](views_functions//materialized_views_refresh.sql)
contains the PostgreSQL statement to refresh the materialized views.

They can be refreshed when the tables are updated by:

- Creating [PostgreSQL triggers](https://www.postgresql.org/docs/current/sql-createtrigger.html)
- Creating a [cron job](https://www.redhat.com/sysadmin/linux-cron-command) or [systemd timer](https://www.freedesktop.org/software/systemd/man/latest/systemd.timer.html)
- Calling them from [descriptorParser](https://gitlab.torproject.org/tpo/network-health/metrics/descriptorParser) (the code that updates the tables)

They're currently being refreshed from [descriptorParser](https://gitlab.torproject.org/tpo/network-health/metrics/descriptorParser/-/blob/d40e47baf3a893dd41fad7c2fa1f7c0ee3f9ef25/src/main/java/org/torproject/metrics/descriptorparser/Main.java#L221)

See more documentation at [docs/](docs/)
