-- Inflation detection DROP views
-- ==============================

DROP MATERIALIZED VIEW IF EXISTS onionperf_nodes CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relay_throughput_ratio CASCADE;
DROP MATERIALIZED VIEW IF EXISTS net_throughput CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relay_throughput_avg_filt CASCADE;

DROP VIEW IF EXISTS relay_throughput_avg_filt CASCADE;
DROP VIEW IF EXISTS relay_throughput_avg CASCADE;
DROP VIEW IF EXISTS relay_throughput CASCADE;
DROP VIEW IF EXISTS as_exits CASCADE;
DROP VIEW IF EXISTS as_guards CASCADE;
DROP VIEW IF EXISTS as_middles CASCADE;
DROP VIEW IF EXISTS fps_as_exit CASCADE;
DROP VIEW IF EXISTS fps_as_middle_or_guard CASCADE;
DROP VIEW IF EXISTS fps_as_guard CASCADE;
DROP VIEW IF EXISTS fps_as_middle CASCADE;

DROP INDEX IF EXISTS relay_throughput_avg_filt_unix_ts_start_onionperf_node_fing_idx;
DROP INDEX IF EXISTS relay_throughput_ratio_unix_ts_start_onionperf_node_fingerp_idx;
DROP INDEX IF EXISTS relay_throughput_ratio_unix_ts_start_idx;
DROP INDEX IF EXISTS relay_throughput_ratio_unix_ts_start_onionperf_node_fingerp_idx;

DROP FUNCTION IF EXISTS throughput_avgs CASCADE;
