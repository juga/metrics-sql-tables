-- Inflation detection flags CREATE views, functions
-- =================================================

--  SELECT * FROM relay_type LIMIT 3;
--       published      |               fingerprint                |               relay_type
-- ---------------------+------------------------------------------+-----------------------------------------
--  2024-03-29 19:15:54 | 218E99F0ABF856F263D4BC4A458665865EEE6B7C | guard
--  2024-03-29 21:10:27 | 51FAFEABB266B7E4E1EB4E6120DF2D7F719157EB | stable_exit_guard
--  2024-03-29 21:52:41 | 6C880630B43354514049DCFEE37A05D80766A72F | guard
CREATE OR REPLACE VIEW relay_type AS
SELECT DISTINCT
    published,
    fingerprint,
    CASE
        WHEN flags LIKE '%BadExit%' THEN 'badexit'
        WHEN flags LIKE '%MiddleOnly%' THEN 'middleonly'
        WHEN flags NOT LIKE '%Guard%'
            AND flags LIKE '%Exit%'
            AND flags NOT LIKE '%Stable%' THEN 'only_exit'
        WHEN flags NOT LIKE '%Guard%'
            AND flags NOT LIKE '%Exit%'
            AND flags LIKE '%Stable%' THEN 'only_stable'
        WHEN flags LIKE '%Guard%'
            AND flags NOT LIKE '%Exit%'
            AND flags LIKE '%Stable%' THEN 'guard'
        WHEN flags  NOT LIKE '%Guard%'
            AND flags LIKE '%Exit%'
            AND flags LIKE '%Stable%' THEN 'stable_exit'
        WHEN flags  LIKE '%Guard%'
            AND flags LIKE '%Exit%'
            AND flags NOT LIKE '%Stable%' THEN 'exit_guard'
        WHEN flags LIKE '%Guard%'
            AND flags LIKE '%Exit%'
            AND flags LIKE '%Stable%' THEN 'stable_exit_guard'
        WHEN flags NOT LIKE '%Guard%'
            AND flags NOT LIKE '%Exit%'
            AND flags NOT LIKE '%Stable%'
            AND flags NOT LIKE '%BadExit%'
            AND flags NOT LIKE '%MiddleOnly%' THEN 'no_guard_exit_stable_middleonly_badexit'
    END relay_type
FROM network_status_entry
GROUP BY fingerprint, published, flags;

-- SELECT * FROM flags LIMIT 3;
--       published      |               fingerprint                | badexit | middleonly | only_exit | only_stable | guard | stable_exit | exit_guard | stable_exit_guard | no_guard_exit_stable_middleonly_badexit
-- ---------------------+------------------------------------------+---------+------------+-----------+-------------+--------------+-------------+------------+-------------------+-----------------------------------------
--  2024-03-29 19:15:54 | 218E99F0ABF856F263D4BC4A458665865EEE6B7C |       0 |          0 |         0 |           0 |            1 |           0 |          0 |                 0 |                                       0
--  2024-03-29 21:10:27 | 51FAFEABB266B7E4E1EB4E6120DF2D7F719157EB |       0 |          0 |         0 |           0 |            0 |           0 |          0 |                 1 |                                       0
--  2024-03-29 21:52:41 | 6C880630B43354514049DCFEE37A05D80766A72F |       0 |          0 |         0 |           0 |            1 |           0 |          0 |                 0 |                                       0
-- Count only 1 for every time it appears in different dates (published, fingerprint)
CREATE OR REPLACE VIEW flags AS
SELECT DISTINCT
    published,
    fingerprint,
    CASE WHEN flags LIKE '%BadExit%' THEN 1 ELSE 0 END AS badexit,
    CASE WHEN flags LIKE'%MiddleOnly%' THEN 1 ELSE 0 END AS middleonly,
    CASE WHEN flags NOT LIKE '%Guard%'
        AND flags LIKE '%Exit%'
        AND flags NOT LIKE '%Stable%' THEN 1 ELSE 0 END AS only_exit,
    CASE WHEN flags NOT LIKE '%Guard%'
        AND flags NOT LIKE '%Exit%'
        AND flags LIKE '%Stable%' THEN 1 ELSE 0 END AS only_stable,
    CASE WHEN flags LIKE '%Guard%'
        AND flags NOT LIKE '%Exit%'
        AND flags LIKE '%Stable%' THEN 1 ELSE 0 END AS guard,
    CASE WHEN flags  NOT LIKE '%Guard%'
        AND flags LIKE '%Exit%'
        AND flags LIKE '%Stable%' THEN 1 ELSE 0 END AS stable_exit,
    CASE WHEN flags  LIKE '%Guard%'
        AND flags LIKE '%Exit%'
        AND flags NOT LIKE '%Stable%' THEN 1 ELSE 0 END AS exit_guard,
    CASE WHEN flags LIKE '%Guard%'
        AND flags LIKE '%Exit%'
        AND flags LIKE '%Stable%' THEN 1 ELSE 0 END AS stable_exit_guard,
    CASE WHEN flags NOT LIKE '%Guard%'
        AND flags NOT LIKE '%Exit%'
        AND flags NOT LIKE '%Stable%'
        AND flags NOT LIKE '%BadExit%'
        AND flags NOT LIKE '%MiddleOnly%'
        THEN 1 ELSE 0 END AS no_guard_exit_stable_middleonly_badexit
FROM network_status_entry
GROUP BY fingerprint, published, flags;

-- Functions
-- ---------

-- SELECT distinct_flags('2024-03-29 21:10:27', '2024-03-29 21:52:41',array['51FAFEABB266B7E4E1EB4E6120DF2D7F719157EB']);
--                         distinct_flags
-- --------------------------------------------------------------
--  (51FAFEABB266B7E4E1EB4E6120DF2D7F719157EB,0,0,0,0,0,0,0,1,0
CREATE OR REPLACE FUNCTION distinct_flags
 (
    datestart TIMESTAMP WITHOUT TIME ZONE,
    dateend TIMESTAMP WITHOUT TIME ZONE,
    fps TEXT []
)
RETURNS TABLE
(
    fingerprint TEXT,
    badexit INT,
    middleonly INT,
    only_exit INT,
    only_stable INT,
    guard INT,
    stable_exit INT,
    exit_guard INT,
    stable_exit_guard INT,
    no_guard_exit_stable_middleonly_badexit INT
) AS
$body$
SELECT DISTINCT
    fingerprint,
    badexit,
    middleonly,
    only_exit,
    only_stable,
    guard,
    stable_exit,
    exit_guard,
    stable_exit_guard,
    no_guard_exit_stable_middleonly_badexit
FROM flags
WHERE fingerprint = any(fps)
AND published BETWEEN datestart AND dateend
$body$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sum_relays_flag (
    datestart TIMESTAMP WITHOUT TIME ZONE,
    dateend TIMESTAMP WITHOUT TIME ZONE,
    fps TEXT []
)
RETURNS TABLE
(
    sum_badexit INT,
    sum_middleonly INT,
    sum_only_exit INT,
    sum_only_stable INT,
    sum_stable_guard INT,
    sum_stable_exit INT,
    sum_stable_exit_guard INT,
    sum_no_guard_exit_stable_middleonly_badexit INT
) AS
$body$
SELECT
    sum(badexit) AS sum_badexit,
    sum(middleonly) AS sum_middleonly,
    sum(only_exit) AS sum_only_exit,
    sum(only_stable) AS sum_only_stable,
    sum(guard) AS sum_guard,
    sum(stable_exit) AS sum_stable_exit,
    sum(stable_exit_guard) AS sum_stable_exit_guard,
    sum(no_guard_exit_stable_middleonly_badexit) AS sum_no_guard_exit_stable_middleonly_badexit
FROM distinct_flags(datestart, dateend, fps)
$body$
LANGUAGE sql;
