-- All the possible hours between two dates

CREATE TEMP TABLE IF NOT EXISTS all_datetimes (dt TIMESTAMP PRIMARY KEY);

CREATE OR REPLACE FUNCTION all_datetimes(
  datestart TIMESTAMP WITHOUT TIME ZONE DEFAULT '2024-03-01',
  dateend TIMESTAMP WITHOUT TIME ZONE DEFAULT now()
) RETURNS TABLE (dt TIMESTAMP WITHOUT TIME ZONE) AS $body$
    INSERT INTO all_datetimes
    SELECT t
    FROM generate_series(
      datestart,
      dateend,
      INTERVAL '1 hour'
    ) AS t(hour);
    SELECT dt FROM all_datetimes;
$body$ LANGUAGE SQL;

-- Select missing hours in `bandwidth_file`
SELECT al.dt AS missing
FROM all_datetimes AS al
LEFT JOIN bandwidth_file bf
ON al.dt = date_trunc('hour', bf.published)
WHERE date_trunc('hour', bf.published) IS NULL
ORDER BY al.dt;

-- Select missing hours in `bandwidth_file_document`
SELECT al.dt AS missing
FROM all_datetimes AS al
LEFT JOIN bandwidth_file bf
ON al.dt = date_trunc('hour', bf.published)
INNER JOIN bandwidth_file_document bfd
ON bfd.bandwidth_file_digest = bf.digest
WHERE date_trunc('hour', bf.published) IS NULL
ORDER BY al.dt;

-- Select missing hours in `network_status`
SELECT al.dt AS missing
FROM all_datetimes AS al
LEFT JOIN network_status ns
ON al.dt = ns.valid_after
INNER JOIN network_status_document nsd
ON nsd.network_status_digest = ns.digest
WHERE ns.valid_after IS NULL
ORDER by missing;
