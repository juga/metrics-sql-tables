-- Relays' Consensus Weight Churn Rate CREATE views, functions
-- ===========================================================

-- Materlialized views are needed to create indexes
-- Any view inherits the indexes of the original tables

-- Relays Consensus Weight Churn Rate (RCWCR) is calculated as:
-- RCWCR = LCW / PSCW
-- LCW: Lost Consensus Weight sum at the end of the period (taking into account
-- their concrete fingerprint)
-- PSCW: Period Start Consensus Weight sum

-- Relays Consensus Weight Retenetion Rate (RCWRR) is calculated as:
-- RCWRR = (PECW - NCW) / PSCW
-- PECW: Period End Consensus Weight sum
-- NCW: New Consensus Weight sum at the end of the period (taking into account
-- their concrete fingerprint)

-- The period is one hour (every consensus)


CREATE OR REPLACE FUNCTION hourly_lost_cw_by_flag (flag TEXT)
RETURNS TABLE (
    end_hour TIMESTAMP WITHOUT TIME ZONE,
    lcw INT
) AS
$body$
SELECT
    hourly_relays_cw_flags.valid_after_hour + INTERVAL '1 hour' AS end_hour,
    sum(hourly_relays_cw_flags.cw) AS lcw
FROM hourly_relays_cw_flags
LEFT JOIN hourly_relays_cw_flags AS end_relays
    ON end_relays.fingerprint = hourly_relays_cw_flags.fingerprint
        AND end_relays.valid_after_hour
        = hourly_relays_cw_flags.valid_after_hour + INTERVAL '1 hour'
WHERE end_relays.fingerprint IS NULL
AND hourly_relays_cw_flags.flags like '%' || flag || '%'
GROUP BY hourly_relays_cw_flags.valid_after_hour
ORDER BY hourly_relays_cw_flags.valid_after_hour
$body$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION hourly_cw_by_flag (flag TEXT)
RETURNS TABLE (
    start_hour TIMESTAMP WITHOUT TIME ZONE,
    end_hour TIMESTAMP WITHOUT TIME ZONE,
    pscw INT
) AS
$body$
SELECT
    valid_after_hour AS start_hour,
    valid_after_hour + INTERVAL '1 hour' AS end_hour,
    sum(cw) AS pscw
FROM hourly_relays_cw_flags
WHERE flags like '%' || flag || '%'
GROUP BY valid_after_hour
ORDER BY valid_after_hour
$body$
LANGUAGE sql;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate AS
SELECT
    hlcw.end_hour,
    hlcw.lcw * 1.0 / hcw.pscw AS cw_churn_lost
FROM hourly_lost_cw_by_flag('') as hlcw
INNER JOIN hourly_cw_by_flag('') as hcw
    ON hlcw.end_hour = hcw.end_hour
ORDER BY hlcw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_guard AS
SELECT
    hlcw.end_hour AS end_hour,
    hlcw.lcw * 1.0 / hcw.pscw AS cw_churn_lost
FROM hourly_lost_cw_by_flag('Guard') AS hlcw
INNER JOIN hourly_cw_by_flag('Guard') AS hcw
    ON hlcw.end_hour = hcw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_exit AS
SELECT
    hlcw.end_hour AS end_hour,
    hlcw.lcw * 1.0 / hcw.pscw AS cw_churn_lost
FROM hourly_lost_cw_by_flag('Exit') AS hlcw
INNER JOIN hourly_cw_by_flag('Exit') AS hcw
    ON hlcw.end_hour = hcw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_hsdir AS
SELECT
    hlcw.end_hour AS end_hour,
    hlcw.lcw * 1.0 / hcw.pscw AS cw_churn_lost
FROM hourly_lost_cw_by_flag('HSDir') AS hlcw
INNER JOIN hourly_cw_by_flag('HSDir') AS hcw
    ON hlcw.end_hour = hcw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_stable AS
SELECT
    hlcw.end_hour AS end_hour,
    hlcw.lcw * 1.0 / hcw.pscw AS cw_churn_lost
FROM hourly_lost_cw_by_flag('Stable') AS hlcw
INNER JOIN hourly_cw_by_flag('Stable') AS hcw
    ON hlcw.end_hour = hcw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_valid AS
SELECT
    hlcw.end_hour AS end_hour,
    hlcw.lcw * 1.0 / hcw.pscw AS cw_churn_lost
FROM hourly_lost_cw_by_flag('Valid') AS hlcw
INNER JOIN hourly_cw_by_flag('Valid') AS hcw
    ON hlcw.end_hour = hcw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_v2dir AS
SELECT
    hlcw.end_hour AS end_hour,
    hlcw.lcw * 1.0 / hcw.pscw AS cw_churn_lost
FROM hourly_lost_cw_by_flag('V2Dir') AS hlcw
INNER JOIN hourly_cw_by_flag('V2Dir') AS hcw
    ON hlcw.end_hour = hcw.end_hour
WITH DATA;

CREATE OR REPLACE FUNCTION hourly_new_cw_by_flag (flag TEXT)
RETURNS TABLE (
    end_hour TIMESTAMP WITHOUT TIME ZONE,
    ncw INT
) AS
$body$
SELECT
    end_relays.valid_after_hour AS end_hour,
    sum(end_relays.cw) AS ncw
FROM hourly_relays_cw_flags
RIGHT JOIN hourly_relays_cw_flags AS end_relays
    ON end_relays.fingerprint = hourly_relays_cw_flags.fingerprint
        AND end_relays.valid_after_hour
        = hourly_relays_cw_flags.valid_after_hour + INTERVAL '1 hour'
WHERE hourly_relays_cw_flags.fingerprint IS NULL
AND end_relays.flags like '%' || flag || '%'
GROUP BY end_relays.valid_after_hour
ORDER BY end_relays.valid_after_hour
$body$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION hourly_start_end_cw_by_flag (flag TEXT)
RETURNS TABLE (
    start_hour TIMESTAMP WITHOUT TIME ZONE,
    end_hour TIMESTAMP WITHOUT TIME ZONE,
    pscw INT,
    pecw INT
) AS
$body$
SELECT
    pscw.start_hour,
    pscw.end_hour,
    pscw.pscw AS pscw,
    pecw.pscw AS pecw
FROM hourly_cw_by_flag('') AS pscw
INNER JOIN hourly_cw_by_flag('') AS pecw
    ON pscw.end_hour = pecw.start_hour
$body$
LANGUAGE sql;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_new AS
SELECT
    hourly_start_end_cw.end_hour,
    hourly_new_cw.ncw * 1.0
    / hourly_start_end_cw.pecw AS cw_churn_new
FROM hourly_new_cw_by_flag('') AS hourly_new_cw
INNER JOIN hourly_start_end_cw_by_flag('') AS hourly_start_end_cw
    ON hourly_new_cw.end_hour = hourly_start_end_cw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_new_guard AS
SELECT
    hsecw.end_hour AS end_hour,
    hncw.ncw * 1.0 / hsecw.pecw AS cw_churn_new
FROM hourly_new_cw_by_flag('Guard') AS hncw
INNER JOIN hourly_start_end_cw_by_flag('Guard') AS hsecw
    ON hncw.end_hour = hsecw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_new_exit AS
SELECT
    hsecw.end_hour AS end_hour,
    hncw.ncw * 1.0 / hsecw.pecw AS cw_churn_new
FROM hourly_new_cw_by_flag('Exit') AS hncw
INNER JOIN hourly_start_end_cw_by_flag('Exit') AS hsecw
    ON hncw.end_hour = hsecw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_new_hsdir AS
SELECT
    hsecw.end_hour AS end_hour,
    hncw.ncw * 1.0 / hsecw.pecw AS cw_churn_new
FROM hourly_new_cw_by_flag('HSDir') AS hncw
INNER JOIN hourly_start_end_cw_by_flag('HSDir') AS hsecw
    ON hncw.end_hour = hsecw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_new_stable AS
SELECT
    hsecw.end_hour AS end_hour,
    hncw.ncw * 1.0 / hsecw.pecw AS cw_churn_new
FROM hourly_new_cw_by_flag('Stable') AS hncw
INNER JOIN hourly_start_end_cw_by_flag('Stable') AS hsecw
    ON hncw.end_hour = hsecw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_new_valid AS
SELECT
    hsecw.end_hour AS end_hour,
    hncw.ncw * 1.0 / hsecw.pecw AS cw_churn_new
FROM hourly_new_cw_by_flag('Valid') AS hncw
INNER JOIN hourly_start_end_cw_by_flag('Valid') AS hsecw
    ON hncw.end_hour = hsecw.end_hour
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS relays_cw_churn_rate_new_v2dir AS
SELECT
    hsecw.end_hour AS end_hour,
    hncw.ncw * 1.0 / hsecw.pecw AS cw_churn_new
FROM hourly_new_cw_by_flag('V2Dir') AS hncw
INNER JOIN hourly_start_end_cw_by_flag('V2Dir') AS hsecw
    ON hncw.end_hour = hsecw.end_hour
WITH DATA;

-- Indexes
-- -------

CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate(end_hour, cw_churn_lost);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_guard_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_guard(end_hour, cw_churn_lost);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_exit_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_exit(end_hour, cw_churn_lost);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_hsdir_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_hsdir(end_hour, cw_churn_lost);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_stable_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_stable(end_hour, cw_churn_lost);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_valid_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_valid(end_hour, cw_churn_lost);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_v2dir_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_v2dir(end_hour, cw_churn_lost);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_new_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new(end_hour, cw_churn_new);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_new_guard_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_guard(end_hour, cw_churn_new);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_new_exit_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_exit(end_hour, cw_churn_new);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_new_hsdidr_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_hsdir(end_hour, cw_churn_new);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_new_stable_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_stable(end_hour, cw_churn_new);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_new_valid_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_valid(end_hour, cw_churn_new);
CREATE UNIQUE INDEX IF NOT EXISTS relays_cw_churn_rate_new_v2dir_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_v2dir(end_hour, cw_churn_new);
