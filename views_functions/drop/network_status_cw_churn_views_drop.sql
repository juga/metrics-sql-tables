-- Relays' Consensus Weight Churn Rate DROP views, functions
-- =========================================================

DROP FUNCTION IF EXISTS hourly_lost_cw_by_flag CASCADE;
DROP FUNCTION IF EXISTS hourly_cw_by_flag CASCADE;

DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate CASCADE;

DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_guard CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_exit CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_hsdir CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_stable CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_valid CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_v2dir CASCADE;

DROP FUNCTION IF EXISTS hourly_new_cw_by_flag CASCADE;
DROP FUNCTION IF EXISTS hourly_start_end_cw_by_flag CASCADE;

DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_new CASCADE;

DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_new_guard CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_new_exit CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_new_hsdir CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_new_stable CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_new_valid CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_cw_churn_rate_new_v2dir CASCADE;


DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate(end_hour, cw_churn_lost);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_guard_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_guard(end_hour, cw_churn_lost);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_exit_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_exit(end_hour, cw_churn_lost);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_hsdir_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_hsdir(end_hour, cw_churn_lost);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_stable_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_stable(end_hour, cw_churn_lost);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_valid_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_valid(end_hour, cw_churn_lost);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_v2dir_end_hour_cw_churn_lost_idx ON relays_cw_churn_rate_v2dir(end_hour, cw_churn_lost);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_new_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new(end_hour, cw_churn_new);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_new_guard_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_guard(end_hour, cw_churn_new);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_new_exit_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_exit(end_hour, cw_churn_new);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_new_hsdidr_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_hsdir(end_hour, cw_churn_new);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_new_stable_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_stable(end_hour, cw_churn_new);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_new_valid_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_valid(end_hour, cw_churn_new);
DROP UNIQUE INDEX IF EXISTS relays_cw_churn_rate_new_v2dir_end_hour_cw_churn_new_idx ON relays_cw_churn_rate_new_v2dir(end_hour, cw_churn_new);
